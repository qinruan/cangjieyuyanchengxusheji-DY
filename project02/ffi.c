#include <stdlib.h>
#include <stdio.h>

// 清屏
void clearScreen() {
    system("clear");
}

// 无需按下回车键，获取一个字符 
char getChar() {

    system("stty raw");
    char ch = getchar();
    system("stty -raw"); 
    return ch;
}
