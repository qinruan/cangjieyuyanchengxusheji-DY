#include <stdio.h>
#include <stdint.h>

// 全局整型变量value，值为20
int64_t value = 20;

// 返回全局整型变量value的指针
int64_t *getInt64Pointer() {
    return &value;

}

// 传入一个整型指针，并输出这个整型值
void outputInt64Value(int64_t *pointer) {
    printf("the value is %ld\n", *pointer);
}

