#include <stdio.h>
#include <stdint.h>

// 通过typedef定义Point2D结构体
typedef struct  {
    int64_t x;
    int64_t y;
} Point2D;

// 两个Point2D类型相加
Point2D add(Point2D p1, Point2D p2) {
    Point2D point2d = {p1.x + p2.x, p1.y + p2.y};
    return point2d;
}
