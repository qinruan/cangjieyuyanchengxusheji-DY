#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

// 打印字符串
void hello() {
    printf("This is from C language.\n");
}

// 加法
int64_t add(int64_t a, int64_t b) {
    return a + b;
}

// 奇数判断
bool isOdd(int64_t value) {
    return value % 2 == 1;
}
