#include <stdio.h>

// 定义函数指针类型callback
typedef void (*callback)(int);

// 传入并调用callback类型的函数
void setCallback(callback cb) {
    cb(20);
}
